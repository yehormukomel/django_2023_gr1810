from django.urls import path, re_path, register_converter
from . import views
from .custom_converter import MyConv

register_converter(MyConv, 'yyyy')

urlpatterns = [
    path('', views.index),
    path('articles/2003/', views.special_case_2003),
    re_path('articles/(?P<year>[0-9]{4})/$', views.year_archive),
    # path('articles/<int:year>/', views.year_archive),
    path('articles/<yyyy:year>/<int:month>/', views.month_archive),
    path('articles/<int:year>/<int:month>/<slug:slug>/', views.article_detail),
    re_path('articles/comments/(?:page-(?P<page_number>[0-9]+)/)?$', views.comments),
    path('blog/', views.blog_page),
    path('blog/page<int:num>/', views.blog_page)
]

